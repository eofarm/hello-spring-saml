package com.example.hellospring.repository;

import java.net.URI;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.TimeZone;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.example.hellospring.entity.AccountEntity;
import com.example.hellospring.model.UserProfile;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, String>
{
    @Modifying
    @Query("UPDATE Account a SET a.active = :active WHERE a.username = :username")
    @Transactional
    void setActive(@Param("username") @NotNull String username, @Param("active") boolean active);
    
    @Modifying
    @Transactional
    default void updateFrom(String username, UserProfile userProfile)
    {
        Assert.notNull(username, "Expected a username");
        Assert.notNull(username, "Expected a user profile");
        Assert.isTrue(username.equals(userProfile.getUsername()), "Expected a profile with same username!");
        
        AccountEntity accountEntity = getOne(username);
        
        accountEntity.setDisplayName(userProfile.getDisplayName());
        accountEntity.setEmail(userProfile.getEmail());
        accountEntity.setLanguage(userProfile.getLanguage());
        
        TimeZone timezone = userProfile.getTimezone();
        accountEntity.setTimezoneId(timezone == null? null : timezone.getID());
    }
    
    @Modifying
    @Transactional
    default void createFrom(
        String username, UserProfile userProfile, boolean active, URI providerUri) 
    {
        Assert.notNull(username, "Expected a username");
        Assert.notNull(username, "Expected a user profile");
        Assert.isTrue(username.equals(userProfile.getUsername()), "Expected a profile with same username!");
        
        final Instant now = Instant.now();
        final TimeZone timezone = userProfile.getTimezone();
        final ZoneId zoneId = timezone == null? ZoneId.systemDefault() : timezone.toZoneId();
        
        AccountEntity accountEntity = new AccountEntity(username);
        accountEntity.setActive(active);
        accountEntity.setDisplayName(userProfile.getDisplayName());
        accountEntity.setRegisteredAt(ZonedDateTime.ofInstant(now, zoneId));
        accountEntity.setEmail(userProfile.getEmail());
        accountEntity.setTimezoneId(zoneId.getId());
        accountEntity.setLanguage(userProfile.getLanguage());
        accountEntity.setIdentityProvider(providerUri == null? null : providerUri.toString());
        save(accountEntity);
    }
}
