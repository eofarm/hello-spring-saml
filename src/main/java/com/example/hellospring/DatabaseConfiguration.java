package com.example.hellospring;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = { "com.example.hellospring.repository" })
@EnableTransactionManagement(mode = AdviceMode.PROXY)
public class DatabaseConfiguration 
{
}
