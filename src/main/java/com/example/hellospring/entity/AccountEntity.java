package com.example.hellospring.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;

import org.hibernate.validator.constraints.URL;

/**
 * Represent an account (either local or remote)
 */
@Entity(name = "Account")
@Table(name = "`account`", schema = "public")
public class AccountEntity
{
    @Id
    @Column(name = "`username`")
    String username;
    
    @Column(name = "`display_name`")
    String displayName;
    
    /** 
     * The password may be <tt>null</tt>, if account is mapped from an external (IdP) account 
     */
    @Column(name = "`password`", nullable = true) 
    String password;
    
    @Column(name = "`active`")
    Boolean active;
    
    @Email
    @NotNull
    @Column(name = "`email`", nullable = false)
    String email;
    
    @NotNull
    @Column(name = "`registered_at`", nullable = false, updatable = false)
    ZonedDateTime registeredAt;
    
    @Pattern(regexp = "^[a-z][a-z]$", flags = Flag.CASE_INSENSITIVE)
    @Column(name = "`language`")
    String language;
    
    @Column(name = "`timezone`")
    String timezoneId;
    
    /**
     * This is the identity provider for this account, or <tt>null</tt> if this account is local.
     */
    @URL
    @Column(name = "`identity_provider`", updatable = false)
    String identityProvider;
    
    public AccountEntity() {}
    
    public AccountEntity(String username) 
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Boolean getActive()
    {
        return active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public ZonedDateTime getRegisteredAt()
    {
        return registeredAt;
    }

    public void setRegisteredAt(ZonedDateTime registeredAt)
    {
        this.registeredAt = registeredAt;
    }
    
    public String getIdentityProvider()
    {
        return identityProvider;
    }

    public void setIdentityProvider(String identityProvider)
    {
        this.identityProvider = identityProvider;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getTimezoneId()
    {
        return timezoneId;
    }

    public void setTimezoneId(String timezoneId)
    {
        this.timezoneId = timezoneId;
    }   
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
}
