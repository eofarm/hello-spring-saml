package com.example.hellospring.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import static java.util.stream.Collectors.toList;

import java.net.URI;
import java.net.URL;

import static java.util.stream.Collectors.collectingAndThen;

public class SimpleUserDetails implements UserDetails, UserProfile
{
    private static final long serialVersionUID = 1L;

    private String username;
    
    /** 
     * The identity provider (IdP), or <tt>null</tt> if this object represents a local account 
     */
    private URI identityProvider;
    
    /** 
     * The list of assigned authorities (roles) 
     */
    private List<GrantedAuthority> authorities = Collections.emptyList();
    
    private String displayName;
    
    private String email;
    
    private String language;
    
    private String timezoneId;
    
    public SimpleUserDetails(String userName)
    {
        this.username = userName;
    }
    
    @Override
    public String getUsername()
    {
        return username;
    }
    
    public void setIdentityProvider(URI uri)
    {
        this.identityProvider = uri;
    }
    
    public URI getIdentityProvider()
    {
        return identityProvider;
    }
    
    @Override
    public String getEmail()
    {
        return email;
    }
    
    @Override
    public String getDisplayName()
    {
        return displayName;
    }
    
    @Override
    public TimeZone getTimezone()
    {
        return timezoneId == null? null : TimeZone.getTimeZone(timezoneId);
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities)
    {
        this.authorities = Collections.unmodifiableList(new ArrayList<>(authorities));
    }
    
    public void setAuthorities(GrantedAuthority ...authorities)
    {
        setAuthorities(Arrays.asList(authorities));
    }
    
    public void setAuthorities(String ...authorities)
    {
        this.authorities = Arrays.stream(authorities)
            .map(SimpleGrantedAuthority::new)
            .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }
    
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setTimezone(TimeZone timezone)
    {
        this.timezoneId = timezone.getID();
    }
    
    @Override
    public String getLanguage()
    {
        return language;
    }
    
    public void setLanguage(String language)
    {
        this.language = language;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return authorities;
    }

    @Override
    public String getPassword()
    {
        // The actual credentials are not present here
        return null;
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @Override
    public boolean isEnabled()
    {
        return true;
    }
    
    @Override
    public String toString()
    {
        return String.format("SimpleUserDetails(username=%s, email=%s, roles=%s)", 
            username, email, getAuthorities());
    }
}
