package com.example.hellospring.model;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.StringUtils;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public enum UserAttribute
{
    USER_NAME(
        "http://wso2.org/claims/username"),
    
    DISPLAY_NAME(
        "http://wso2.org/claims/displayName"),
    
    EMAIL(
        "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress",
        "http://wso2.org/claims/emailaddress",
        "http://axschema.org/contact/email"),
    
    LANGUAGE(
        "http://axschema.org/pref/language",
        "http://wso2.org/claims/preferredLanguage"),
    
    TIMEZONE(
        "http://axschema.org/pref/timezone",
        "http://wso2.org/claims/timeZone");
    
    private final List<URI> attrs;
    
    private static Map<URI, UserAttribute> map;
    
    private UserAttribute(String... attrs)
    {
        this.attrs = Arrays.stream(attrs).map(URI::create)
            .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }
    
    public List<URI> getUris()
    {
        return attrs;
    }
    
    public static UserAttribute fromUri(String attr)
    {
        if (StringUtils.isEmpty(attr))
            throw new IllegalArgumentException("A non-empty attribute is needed");
        return UserAttribute.fromUri(URI.create(attr));
    }
    
    public static UserAttribute fromUri(URI attr)
    {
        if (attr == null)
            throw new IllegalArgumentException("A non-null attribute URI is needed");
        return map.get(attr);
    }
    
    static {
        map = new HashMap<>();
        for (UserAttribute e: UserAttribute.values()) {
            for (URI uri: e.attrs) {
                UserAttribute e1 = map.putIfAbsent(uri, e);
                if (e1 != null) {
                    throw new IllegalStateException(
                        "The URI [" + uri + "] is associated with more than one constants");
                }
            }
        }
    }
}
