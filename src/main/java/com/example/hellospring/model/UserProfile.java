package com.example.hellospring.model;

import java.util.TimeZone;

public interface UserProfile
{
    String getUsername();
    
    String getDisplayName();
    
    String getEmail();
    
    String getLanguage();
    
    default TimeZone getTimezone() { return TimeZone.getDefault(); };
}
