package com.example.hellospring;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class KeyStoreConfiguration
{
    @Value("${hellospring.keystore.file:config/keystore.jks}")
    private Resource keystoreResource;
    
    @Value("${hellospring.keystore.password:}")
    private String password;
    
    @Bean
    public KeyStore keystore() 
        throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException
    {
        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        
        try (InputStream in = keystoreResource.getInputStream()) {
            keystore.load(in, password.toCharArray());
        }
        
        return keystore;
    }
}
