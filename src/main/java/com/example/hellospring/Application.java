package com.example.hellospring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
    // Todo: is this explicit scan needed?
    // taken from https://github.com/spring-projects/spring-security-saml/blob/1.0.3.RELEASE/sample/src/main/webapp/WEB-INF/securityContext.xml#L13
    scanBasePackages = {
        "com.example.hellospring",
        "org.springframework.security.saml",
    }
)
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
