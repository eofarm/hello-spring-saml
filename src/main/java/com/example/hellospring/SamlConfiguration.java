package com.example.hellospring;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.velocity.app.VelocityEngine;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.metadata.provider.FilesystemMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.security.saml.SAMLAuthenticationProvider;
import org.springframework.security.saml.SAMLBootstrap;
import org.springframework.security.saml.context.SAMLContextProvider;
import org.springframework.security.saml.context.SAMLContextProviderImpl;
import org.springframework.security.saml.key.JKSKeyManager;
import org.springframework.security.saml.key.KeyManager;
import org.springframework.security.saml.log.SAMLDefaultLogger;
import org.springframework.security.saml.metadata.CachingMetadataManager;
import org.springframework.security.saml.metadata.MetadataDisplayFilter;
import org.springframework.security.saml.metadata.MetadataGenerator;
import org.springframework.security.saml.metadata.MetadataGeneratorFilter;
import org.springframework.security.saml.metadata.MetadataManager;
import org.springframework.security.saml.parser.ParserPoolHolder;
import org.springframework.security.saml.processor.HTTPArtifactBinding;
import org.springframework.security.saml.processor.HTTPPAOS11Binding;
import org.springframework.security.saml.processor.HTTPPostBinding;
import org.springframework.security.saml.processor.HTTPRedirectDeflateBinding;
import org.springframework.security.saml.processor.HTTPSOAP11Binding;
import org.springframework.security.saml.processor.SAMLProcessor;
import org.springframework.security.saml.processor.SAMLProcessorImpl;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.security.saml.util.VelocityFactory;
import org.springframework.security.saml.websso.ArtifactResolutionProfile;
import org.springframework.security.saml.websso.ArtifactResolutionProfileImpl;
import org.springframework.security.saml.websso.WebSSOProfile;
import org.springframework.security.saml.websso.WebSSOProfileConsumer;
import org.springframework.security.saml.websso.WebSSOProfileConsumerHoKImpl;
import org.springframework.security.saml.websso.WebSSOProfileConsumerImpl;
import org.springframework.security.saml.websso.WebSSOProfileImpl;
import org.springframework.security.saml.websso.WebSSOProfileOptions;

@Configuration
public class SamlConfiguration
{
    //
    // This is java configuration derived from: 
    // https://github.com/spring-projects/spring-security-saml/blob/1.0.3.RELEASE/sample/src/main/webapp/WEB-INF/securityContext.xml
    
    @Bean
    SAMLBootstrap samlBootstrap() 
    {
        return new SAMLBootstrap();
    }
    
    @Bean
    SAMLDefaultLogger samlLogger()
    {
        return new SAMLDefaultLogger();
    }
    
    @Bean
    VelocityEngine samlVelocityEngine()
    {
        return VelocityFactory.getEngine();
    }
    
    @Bean
    StaticBasicParserPool samlParserPool() throws XMLParserException
    {
        StaticBasicParserPool parser = new StaticBasicParserPool();
        parser.initialize();
        return parser;
    }
    
    @Bean
    ParserPoolHolder samlParserPoolHolder()
    {
        return new ParserPoolHolder();
    }
    
    @Bean
    HTTPPostBinding samlHttpPostBinding(ParserPool samlParserPool, VelocityEngine samlVelocityEngine)
    {
        return new HTTPPostBinding(samlParserPool, samlVelocityEngine);
    }
    
    @Bean
    HTTPRedirectDeflateBinding samlHttpRedirectBinding(ParserPool samlParserPool)
    {
        return new HTTPRedirectDeflateBinding(samlParserPool);
    }
    
    @Bean
    HTTPArtifactBinding samlHttpArtifactBinding(
        HttpConnectionManager httpConnectionManager,
        ParserPool samlParserPool, 
        VelocityEngine samlVelocityEngine)
    {
        HttpClient httpClient = new HttpClient(httpConnectionManager);
        ArtifactResolutionProfile artifactProfile = new ArtifactResolutionProfileImpl(httpClient);   
        return new HTTPArtifactBinding(samlParserPool, samlVelocityEngine, artifactProfile);
    }
    
    @Bean
    HTTPPAOS11Binding samlHttpPaosBinding(ParserPool samlParserPool)
    {
        return new HTTPPAOS11Binding(samlParserPool);
    }
    
    @Bean
    HTTPSOAP11Binding samlHttpSoapBinding(ParserPool samlParserPool)
    {
        return new HTTPSOAP11Binding(samlParserPool);
    }
    
    @Bean
    SAMLContextProvider samlContextProvider()
    {
        return new SAMLContextProviderImpl();
    }
        
    @Bean
    WebSSOProfileOptions samlWebSsoProfileOptions()
    {
        WebSSOProfileOptions options = new WebSSOProfileOptions();
        options.setNameID(NameID.EMAIL);
        options.setIncludeScoping(false);
        return options;
    }
    
    @Bean({"webSsoProfileConsumer", "webSSOprofileConsumer"})
    WebSSOProfileConsumer webSsoProfileConsumer()
    {
        return new WebSSOProfileConsumerImpl();
    }
     
    @Bean({"webSsoProfile", "webSSOprofile"})
    WebSSOProfile webSsoProfile()
    {
        return new WebSSOProfileImpl();
    }
    
    @Bean({"webSsoHokProfileConsumer", "hokWebSSOprofileConsumer"})
    WebSSOProfileConsumer webSsoHokProfileConsumer()
    {
        return new WebSSOProfileConsumerHoKImpl();
    }
    
    @Bean({"webSsoHokProfile", "hokWebSSOprofile"})
    WebSSOProfile webSsoHokProfile()
    {
        return new WebSSOProfileImpl();
    }
    
    /** 
     * The SAML processor is responsible for loading incoming SAML messages from HTTP 
     * request. 
     */
    @Bean
    SAMLProcessor samlProcessor(
        HTTPRedirectDeflateBinding samlHttpRedirectBinding, 
        HTTPPostBinding samlHttpPostBinding, 
        HTTPSOAP11Binding samlHttpSoapBinding,
        HTTPPAOS11Binding samlHttpPaosBinding,
        HTTPArtifactBinding samlHttpArtifactBinding)
    {
        return new SAMLProcessorImpl(Arrays.asList(
            samlHttpRedirectBinding, 
            samlHttpPostBinding, 
            samlHttpSoapBinding, 
            samlHttpPaosBinding, 
            samlHttpArtifactBinding));
    }
       
    @Bean
    List<MetadataProvider> metadataForIdentityProviders(
        ParserPool samlParserPool, Map<String, Resource> configurationForIdentityProviders) 
        throws MetadataProviderException, IOException
    {
        List<MetadataProvider> providers = new ArrayList<>();
        for (Resource r: configurationForIdentityProviders.values()) {
            FilesystemMetadataProvider provider = new FilesystemMetadataProvider(r.getFile());
            provider.setParserPool(samlParserPool);
            providers.add(provider);
        }
        return providers;
    }
    
    @Bean
    @ConfigurationProperties(prefix = "hellospring.saml.identity-providers", ignoreUnknownFields = true)
    Map<String, Resource> configurationForIdentityProviders()
    {
        return new HashMap<>();
    }
    
    @Bean
    KeyManager keyManager(
        KeyStore keystore, 
        @Value("${hellospring.keystore.password:}") final String password,
        @Value("${hellospring.keystore.default-key:mykey}") final String defaultKey)
    {   
        return new JKSKeyManager(keystore, Collections.singletonMap(defaultKey, password), defaultKey);
    }
    
    @Bean
    public MetadataManager samlMetadataManager(
            KeyManager keyManager, 
            List<MetadataProvider> metadataForIdentityProviders,
            @Value("${hellospring.saml.default-identity-provider:}") URL defaultIdentityProviderUrl) 
        throws MetadataProviderException, IOException
    {       
        CachingMetadataManager manager = new CachingMetadataManager(metadataForIdentityProviders);
        manager.setKeyManager(keyManager);
        manager.setRefreshRequired(false);
        if (defaultIdentityProviderUrl != null)
            manager.setDefaultIDP(defaultIdentityProviderUrl.toString());
        return manager;
    }
    
    @Bean
    SAMLAuthenticationProvider samlAuthenticationProvider(SAMLUserDetailsService userDetailsService)
    {
        SAMLAuthenticationProvider samlAuthenticationProvider = new SAMLAuthenticationProvider();
        samlAuthenticationProvider.setUserDetails(userDetailsService);
        return samlAuthenticationProvider;
    }
    
    //
    // Register (non security-related) servlet filters
    // Note: The security-related filters are added to Spring-Security filter chain
    //
    
    @Bean
    FilterRegistrationBean<MetadataGeneratorFilter> samlMetadataGeneratorFilter(
        KeyManager keyManager,
        MetadataManager metadataManager,
        @Value("${hellospring.saml.service-provider.base-url}") URL baseUrl) 
        throws MalformedURLException
    {
        URL entityUrl = new URL(baseUrl, "SAML2");
        
        MetadataGenerator gen = new MetadataGenerator();
        gen.setEntityBaseURL(baseUrl.toString().replaceFirst("/$", ""));
        gen.setEntityId(entityUrl.toString());
        gen.setId(entityUrl.toString());
        gen.setKeyManager(keyManager);
        
        MetadataGeneratorFilter filter = new MetadataGeneratorFilter(gen);
        filter.setManager(metadataManager);
        
        FilterRegistrationBean<MetadataGeneratorFilter> registerFilter = 
            new FilterRegistrationBean<>(filter);
        registerFilter.setOrder(-101); // before security filter chain (at -100)
        
        return registerFilter;
    }
    
    @Bean
    FilterRegistrationBean<MetadataDisplayFilter> samlMetadataDisplayFilter(
        SAMLContextProvider contextProvider,
        KeyManager keyManager,
        MetadataManager metadataManager)
    {
        MetadataDisplayFilter filter = new MetadataDisplayFilter();
        filter.setContextProvider(contextProvider);
        filter.setKeyManager(keyManager);
        filter.setManager(metadataManager);
        
        FilterRegistrationBean<MetadataDisplayFilter> registerFilter = 
            new FilterRegistrationBean<>(filter);
        registerFilter.setUrlPatterns(Arrays.asList("/saml/metadata"));
        registerFilter.setOrder(+1000); // order doesn't matter here
        
        return registerFilter;
    }
}
