package com.example.hellospring;

import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HttpClientConfiguration
{
    @Bean
    MultiThreadedHttpConnectionManager httpConnectionManager()
    {
        MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        return connectionManager;
    }
}
