package com.example.hellospring.service;

import java.net.URI;
import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.example.hellospring.model.SimpleUserDetails;
import com.example.hellospring.repository.AccountRepository;

/**
 * An application-level listener for successful authentications events. 
 * <p>
 * These events can be used to:
 * <ul>
 *   <li>auto-register (local) accounts when a remote user is authenticated (via SAML SSO) for 1st time 
 *   <li>update account info from attributes received from an IdP
 * </ul> 
 */
@Service
public class DefaultAccountUpdateService implements ApplicationListener<AuthenticationSuccessEvent>
{
    @Autowired
    AccountRepository accountRepository;
    
    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event)
    {
        final Authentication authn = event.getAuthentication();
        final Instant now = Instant.ofEpochMilli(event.getTimestamp());
        final String username = authn.getName();
        
        final Object details = authn.getDetails(); 
        final Object credentials = authn.getCredentials();
        
        if (details instanceof SimpleUserDetails) {
            final SimpleUserDetails userDetails = (SimpleUserDetails) details;
            if (userDetails.getIdentityProvider() != null) {
                // This is an "external" user authenticated by an IdP
                Assert.state(credentials == null || (credentials instanceof SAMLCredential), 
                    "Expected credentials from a SAML login!");
                final URI providerUri = userDetails.getIdentityProvider();
                Assert.state(providerUri != null, "Expected a non-null URI for identity provider!");
                if (accountRepository.existsById(username)) {
                    // Update account information
                    accountRepository.updateFrom(username, userDetails);
                } else {
                    // Create a new local account to correspond to this user
                    accountRepository.createFrom(username, userDetails, true, providerUri);
                }
            }
        }
    }
}
