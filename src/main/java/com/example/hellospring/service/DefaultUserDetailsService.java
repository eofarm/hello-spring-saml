package com.example.hellospring.service;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.TimeZone;
import java.util.stream.Stream;

import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.Subject;
import org.opensaml.xml.schema.XSString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.stereotype.Service;

import com.example.hellospring.model.UserAttribute;
import com.example.hellospring.model.SimpleUserDetails;

@Service
public class DefaultUserDetailsService implements SAMLUserDetailsService
{
    private static final Logger logger = LoggerFactory.getLogger(DefaultUserDetailsService.class);
    
    @Override
    public SimpleUserDetails loadUserBySAML(SAMLCredential credentials) 
        throws UsernameNotFoundException
    {
        final Subject subject = credentials.getAuthenticationAssertion().getSubject();
        final String username = subject.getNameID().getValue();
        
        final SimpleUserDetails userDetails = new SimpleUserDetails(username);
        
        final String issuer = credentials.getAuthenticationAssertion().getIssuer().getValue();
        userDetails.setIdentityProvider(URI.create(issuer));
       
        // Map SAML attributes to properties of a UserDetails object
        
        for (Attribute attr: credentials.getAttributes()) {
            final UserAttribute userAttr = UserAttribute.fromUri(attr.getName());
            if (userAttr == null) {
                logger.debug("Ignoring unknown attribute: {}", attr.getName());
                continue;
            }
            logger.debug("Mapping known attribute: {}", attr.getName());
            try (Stream<String> values = attr.getAttributeValues().stream()
                    .map(x -> ((XSString) x).getValue())) 
            {
                switch (userAttr) {
                case DISPLAY_NAME:
                    if (userDetails.getDisplayName() == null)
                        userDetails.setDisplayName(values.findFirst().get());
                    break;
                case EMAIL:
                    if (userDetails.getEmail() == null) 
                        userDetails.setEmail(values.findFirst().get());
                    break;
                case TIMEZONE:
                    if (userDetails.getTimezone() == null)
                        userDetails.setTimezone(TimeZone.getTimeZone(values.findFirst().get()));
                    break;
                case LANGUAGE:
                    if (userDetails.getLanguage() == null)
                        userDetails.setLanguage(values.findFirst().get());
                default:
                    break;
                }
            }
        }
        
        // Assign any authorities (i.e. roles) mapped from SAML credentials
         
        userDetails.setAuthorities("ROLE_USER", "ROLE_EXTERNAL_USER");
        
        return userDetails;
    }
}
