package com.example.hellospring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.example.hellospring.model.SimpleUserDetails;

@Controller
public class HomeController
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    
    @ModelAttribute
    void addUserInfo(Model model, Authentication authn)
    {
        Assert.state(authn != null, "Did not expect a null authentication object!");
        
        final String username = authn.getName();
        model.addAttribute("username", username);
        
        // Depending on the authentication method, principal will have a different type
        
        UserDetails userdetails = null;
        final Object principal = authn.getPrincipal();
        final Object details = authn.getDetails();
        if (principal instanceof UserDetails)
            userdetails = (UserDetails) principal;
        else if (details instanceof UserDetails)
            userdetails = (UserDetails) details;
        else
            userdetails = new SimpleUserDetails(username);
        
        model.addAttribute("userdetails", userdetails);
    }
    
    @GetMapping({"/home"})
    public String home(Authentication authn, Model model)
    {
        logger.info("home(): name={}, authorities={}, principal={}, details={}", 
            authn.getName(), authn.getAuthorities(), authn.getPrincipal(), authn.getDetails());
        
        return "home";
    }
}
