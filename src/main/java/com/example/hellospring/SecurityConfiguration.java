package com.example.hellospring;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLAuthenticationProvider;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.SAMLProcessingFilter;
import org.springframework.security.saml.SAMLWebSSOHoKProcessingFilter;
import org.springframework.security.saml.context.SAMLContextProvider;
import org.springframework.security.saml.log.SAMLLogger;
import org.springframework.security.saml.metadata.MetadataManager;
import org.springframework.security.saml.processor.SAMLProcessor;
import org.springframework.security.saml.websso.WebSSOProfile;
import org.springframework.security.saml.websso.WebSSOProfileOptions;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.authentication.DelegatingAuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{ 
    @Autowired
    SAMLLogger samlLogger;
    
    @Autowired
    SAMLContextProvider samlContextProvider;
    
    @Autowired
    SAMLProcessor samlProcessor;
    
    @Autowired
    MetadataManager samlMetadataManager;
    
    @Autowired
    WebSSOProfileOptions webSsoProfileOptions;
    
    @Autowired
    WebSSOProfile webSsoProfile;
    
    @Autowired
    WebSSOProfile webSsoHokProfile;
    
    @Autowired
    SAMLAuthenticationProvider samlAuthenticationProvider;
    
    @Bean
    SimpleUrlAuthenticationSuccessHandler successLoginHandler()
    {
        SavedRequestAwareAuthenticationSuccessHandler successLoginHandler = 
            new SavedRequestAwareAuthenticationSuccessHandler();
        successLoginHandler.setDefaultTargetUrl("/");
        return successLoginHandler;
    }
    
    @Bean
    SimpleUrlAuthenticationFailureHandler failureLoginHandler()
    {
        SimpleUrlAuthenticationFailureHandler failureLoginHandler = 
            new SimpleUrlAuthenticationFailureHandler();
        failureLoginHandler.setDefaultFailureUrl("/login?error");
        return failureLoginHandler;
    }
    
    @Bean
    AuthenticationEntryPoint authenticationEntryPoint()
    {
        final LinkedHashMap<RequestMatcher, AuthenticationEntryPoint> map = 
            new LinkedHashMap<RequestMatcher, AuthenticationEntryPoint>();

        map.put(new AntPathRequestMatcher("/api/**"), new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));

        final DelegatingAuthenticationEntryPoint entryPoint = new DelegatingAuthenticationEntryPoint(map);
        entryPoint.setDefaultEntryPoint(new LoginUrlAuthenticationEntryPoint("/saml/login"));

        return entryPoint;
    }
    
    SAMLEntryPoint samlEntryPoint()
    {
        SAMLEntryPoint entryPoint = new SAMLEntryPoint();
        entryPoint.setContextProvider(samlContextProvider);
        entryPoint.setSamlLogger(samlLogger);
        entryPoint.setWebSSOprofile(webSsoProfile);
        entryPoint.setWebSSOprofileHoK(webSsoHokProfile);
        entryPoint.setMetadata(samlMetadataManager);
        entryPoint.setDefaultProfileOptions(webSsoProfileOptions);
        return entryPoint;
    }
    
    SAMLProcessingFilter webSsoProcessingFilter() throws Exception
    {
        SAMLProcessingFilter filter = new SAMLProcessingFilter();
        filter.setSAMLProcessor(samlProcessor);
        filter.setContextProvider(samlContextProvider);
        filter.setAuthenticationSuccessHandler(this.successLoginHandler());
        filter.setAuthenticationFailureHandler(this.failureLoginHandler());
        filter.setAuthenticationManager(this.authenticationManager());
        return filter;
    }
    
    SAMLProcessingFilter webSsoHokProcessingFilter() throws Exception
    {
        SAMLProcessingFilter filter = new SAMLWebSSOHoKProcessingFilter();
        filter.setSAMLProcessor(samlProcessor);
        filter.setContextProvider(samlContextProvider);
        filter.setAuthenticationSuccessHandler(this.successLoginHandler());
        filter.setAuthenticationFailureHandler(this.failureLoginHandler());
        filter.setAuthenticationManager(this.authenticationManager());
        return filter;
    }
        
    @Override
    public void configure(WebSecurity security) throws Exception
    {
        security.ignoring().antMatchers("/css/*", "/images/*", "/js/*");
    }
    
    @Override
    protected void configure(HttpSecurity security) throws Exception
    {
        security.authorizeRequests()
            .antMatchers(
                    "/", "/index",
                    "/scratch/**",
                    "/saml/metadata", "/saml/SSO", "/saml/login")
                .permitAll()
            .antMatchers(HttpMethod.GET, "/logged-out")
                .permitAll()
            .antMatchers(HttpMethod.GET, "/home/**")
                .hasRole("USER")
            .antMatchers(HttpMethod.GET, "/api/**")
                .authenticated()
            .antMatchers(HttpMethod.POST, "/api/**")
                .authenticated()
            .antMatchers(HttpMethod.PUT, "/api/**")
                .authenticated()
            .antMatchers(HttpMethod.DELETE, "/api/**")
                .authenticated();
        
        FilterChainProxy samlSecurityFilter = new FilterChainProxy(Arrays.asList(
            new DefaultSecurityFilterChain(
                new AntPathRequestMatcher("/saml/login/**"), this.samlEntryPoint()),
            new DefaultSecurityFilterChain(
                new AntPathRequestMatcher("/saml/SSO/**"), this.webSsoProcessingFilter()),
            new DefaultSecurityFilterChain(
                new AntPathRequestMatcher("/saml/SSOHok/**"), this.webSsoHokProcessingFilter())));
        security.addFilterAfter(samlSecurityFilter, BasicAuthenticationFilter.class);
        
        // If not authenticated, define authentication entry points for different paths
        security.exceptionHandling()
            .authenticationEntryPoint(this.authenticationEntryPoint());
        
        // Todo Also support local form-based login
        
        security.logout()
            .logoutUrl("/logout")
            .logoutSuccessUrl("/logged-out")
            .clearAuthentication(true)
            .invalidateHttpSession(true);       
        
        // Configure where CSRF protection is required
        security.csrf()
            .ignoringAntMatchers("/api/**", "/saml/SSO", "/h2/**");
        
        // Allow to be framed by same-origin (basically for H2 console)
        security.headers().frameOptions().sameOrigin();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    { 
        auth.authenticationProvider(samlAuthenticationProvider);
    }
}