CREATE TABLE public."account" (
    "username" VARCHAR(64) PRIMARY KEY,
    "password" VARCHAR(64),
    "active" BOOLEAN,
    "display_name" VARCHAR(128),
    "email" VARCHAR(64) NOT NULL UNIQUE,
    "registered_at" TIMESTAMP WITH TIME ZONE NOT NULL,
    "language" VARCHAR(2) DEFAULT 'en',
    "timezone" VARCHAR,  
    "identity_provider" VARCHAR);
