# README

Create needed directories:

    mkdir config data temp

Copy and tweak basic configuration under `src/main/resources/config`:

    cp -v config-example/*.properties src/main/resources/config/

## 1. Create SP key/certificate pair 

Generate a key/certificate pair for this service provider (SP). For example:

    keytool -genkey -keyalg RSA -keystore config/keystore.jks -keysize 2048 -storepass secret -keypass secret \
        -dname 'CN=Michail Alexakis, OU=devel, O=Baz, L=Athens, ST=Greece, C=GR'

## 2. Create server key/certificate pair

Generate a key/certificate pair for this HTTPS server. For example (be sure to use proper name!):

    keytool -genkey -keyalg RSA -keystore config/server.jks -keysize 2048 -alias server -storepass secret -keypass secret \
         -dname 'CN=foo.helloworld.localdomain, OU=devel, O=Foo, L=Athens, ST=Greece, C=GR'

## 3. Configure SP

Create a local configuration under `config/application.properties`. For example:

```ini
#
# Server
#

server.port = 8443

security.require-ssl = true
server.ssl.enabled = true
server.ssl.key-store = file:config/server.jks
server.ssl.key-store-password = secret
server.ssl.key-alias = server

#
# SAML authentication
#

hellospring.keystore.file = file:config/keystore.jks
hellospring.keystore.password = secret

hellospring.saml.service-provider.base-url = https://foo.helloworld.localdomain:8443
hellospring.saml.default-identity-provider = https://nowhere-land.ddns.net

hellospring.saml.identity-providers.nowhere-land = file:config/saml/metadata/nowhere-land.ddns.net/idp.xml

## or access SAML metadata directly as URLs ...
#hellospring.saml.identity-providers.nowhere-land = https://nowhere-land.ddns.net/identity/metadata/saml2
```

Download and store IdP metadata under `config/saml/metadata/<idp-name>/idp.xml`. 

Here, we use a demo IdP at `nowhere-land.ddns.net`, so:
    
    mkdir -p config/saml/metadata/nowhere-land.ddns.net

An IdP usually advertises its XML metadata at some standard public location. For example, for `nowhere-land.ddns.net` which is a WSO2IS-based IdP:
    
    curl https://nowhere-land.ddns.net/identity/metadata/saml2 > config/saml/metadata/nowhere-land.ddns.net/idp.xml

## 4. Initialize database

Create database schema from `src/main/resources/sql`. 

For example, if running on an embedded H2 database:
    
    mvn dependency:copy-dependencies -DincludeGroupIds=com.h2database
    java -cp target/dependency/h2-1.4.197.jar org.h2.tools.RunScript -user sa -url jdbc:h2:./data/development -script src/main/resources/sql/initialize.sql


## 5. Publish SP metadata to IdP

The IdP must also known (in advance) certain information for each SP that will communicate with. So, the SP also advertises itself via XML metadata.  

Our Spring-based SP publishes its metadata under `/saml/metadata`.
    
    curl -k https://foo.helloworld.localdomain:8443/saml/metadata > temp/foo-helloworld-localdomain.xml

    
